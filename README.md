# RootCaOnOpenSSL

Build your own Root CA on OpenSSL project

Generuj priv klíč k CA:
Pozn.: Windows neumí pořádně pracovat s ed25519 křivkami - certifikáty jsou sice naimportovány, ale důvěryhodnost nefunguje.

openssl genpkey -algorithm Ed25519 -pass pass:Testik  -out ed25519key_rootca.pem

Generuj z priv CA klíče pub key s parametry na které se zeptá:

openssl.exe req -x509 -new -nodes -key ed25519key_rootca.pem -sha256 -days 30000 -out rootca_public.pem


Další klíč pro tvoji službu:

openssl genpkey -algorithm Ed25519 -pass pass:test -out ed25519key_moje_služby.key

Vytoření CSR:

openssl req -new -key ed25519key_moje_služby.key -out ed25519key_moje_služby.csr

Nyní musíme vytvořit konfigurační soubor kvůli subjectAltName:

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = vpn.nejeral.cz

Teď vytvoříme certifikát:

openssl x509 -req -in ed25519key_moje_služby.csr -CA rootca_public.cer -CAkey ed25519key_rootca.pem -CAcreateserial -out ed25519key_moje_služby.cer -days 825 -extfile ed25519key_moje_služby.ext

Tímto jej zkonvertujeme do p12 formátu i s CA:

openssl pkcs12 -export -in cert -inkey key -certfile ca -name MyClient -out client.p12

